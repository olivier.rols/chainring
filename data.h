#ifndef DATA_H_INCLUDED
#define DATA_H_INCLUDED

/* data configuration types */
#define CONF_DEFAULT 1
#define CONF_FILE (CONF_DEFAULT+1)

/* max line size of config file */
#define LINE_MAX_SIZE 1024

typedef struct DATA {
	/* general link to program data */
	t_chring* chring_ptr;
	char config_type;
	char* config_filename;
	char output_filename[100];
	char** keylist;
} t_data;

int get_data(t_data* data_ptr);
int config_parse(t_data* data_ptr);
int data_extract (t_data* data_ptr,char* buf, int eqpos);

#endif /* DATA_H_INCLUDED */
