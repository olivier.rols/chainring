#include <stdio.h>
#include <math.h>

#include "tooth.h"
#include "dxf.h"
#include "ring.h"
#include "itc.h"
#include "radius.h"

#define FALSPOS_ITER 100
#define IW_MAX 100
#define ERR_RADIUS 1e-12
#define ERR_POSI 1e-12
#define ROUND_SCALE 1000000

double long_curvi(double alpha1,double alpha2,t_chring* chring_ptr) {
	/* arc signed length computation along curve between 2 angles
	for a given scale factor (radius != 1)
	*/
	double alpha,dalpha,dist;
	int i;

	dalpha = (alpha2-alpha1)/CURVISTEP;
	alpha = alpha1;
	dist=0;
	for (i=0; i<CURVISTEP; i++) {
		dist = dist+ chring_ptr->scale * radius_n(alpha,chring_ptr) * dalpha;
		alpha = alpha+dalpha;
	}
	return dist;
}

double dist_curvi (double alpha1,double alpha2,t_chring* chring_ptr) {
	/* curvilinear distance signed computation along curve between 2 angles
	for a given scale factor (radius != 1)
	*/
	double dist;

	/* choose smallest angle difference	i.e.
	if différence is > than PI then "one turn backward"	alpha2
	*/
	if (alpha2-alpha1 > M_PI) {
		alpha2 = alpha2 - 2*M_PI;
	}
	dist = long_curvi(alpha1,alpha2,chring_ptr);
	return dist;
}

double dist_straight (double alpha1,double alpha2,t_chring* chring_ptr) {
	/* geometric distance computation along curve between 2 points
	for 2 angles and a given scale factor (radius != 1)
	*/
	double dist,r1,x1,y1,r2,x2,y2;

	r1 = radius_n(alpha1,chring_ptr);
	x1 = r1 * chring_ptr->scale * cos(alpha1);
	y1 = r1 * chring_ptr->scale * sin(alpha1);

	r2 = radius_n(alpha2,chring_ptr);
	x2 = r2 * chring_ptr->scale * cos(alpha2);
	y2 = r2 * chring_ptr->scale * sin(alpha2);

	dist = sqrt(pow(x1-x2, 2)+pow(y1-y2, 2));;
	return dist;
}

int itc_update (int idx, double alpha, t_chring* chring_ptr) {
	/* itc (intra teeth circle) update, given corresponding idx index and alpha angle
	Beware ! all itcs and angle table are supposed to have been allocated in memory*/
	double x,y,r;

	r = radius_n(alpha,chring_ptr) * chring_ptr->scale;
	x = r * cos(alpha);
	y = r * sin(alpha);
	(chring_ptr->itc_ptr+idx)->x = x;
	(chring_ptr->itc_ptr+idx)->y = y;
	(chring_ptr->itc_ptr+idx)->r = r;
	(chring_ptr->itc_ptr+idx)->alpha = alpha;
	(chring_ptr->itc_ptr+idx)->idx = idx;
	return 0;
}

double itc_next (int idx,t_chring* chring_ptr) {
	/* next itc (idx +1 index) computation so as
    distance between idx and idx+1 itcs equals chain step value
	Beware ! next itc is supposed to have been allocated in memory
	returned value is the result of optimisation residual error value
	*/
	double alphaD,alpha_a,alpha_b,alpha_c,dalpha;
	double e_a,e_b,e_c;
	int nb_iter,iw;

	/* optimisation is performed by false position method (c.f. Wikipedia) */

	/* parameters initialisation is performed so as they lead to an error function
	near "both sides of one zero" */
	alphaD = (chring_ptr->itc_ptr+idx)->alpha;
	if ((chring_ptr->itc_ptr+idx)->r == 0) {
		printf("Erreur itc_next\n");
		return NAN;
	}
	alpha_c = alphaD + CHAIN_STEP / (chring_ptr->itc_ptr+idx)->r;
	dalpha = alpha_c - alphaD;

	e_c = dist_straight(alphaD,alpha_c,chring_ptr) - CHAIN_STEP;
	iw = 0;
	while (e_c < 0) {
		alpha_c = alpha_c + dalpha;
		e_c = dist_straight(alphaD,alpha_c,chring_ptr) - CHAIN_STEP;
		iw++;
		if (iw>IW_MAX) { /* avoid infinited loop ! */
			return ERR_TUN;
		}
	}
	alpha_a = alpha_c;
	e_a = e_c;
	alpha_b = alphaD;
	e_b = -CHAIN_STEP;

	/* check that e_a and e_b are signed different */
	if (e_a*e_b>=0) {
		return ERR_TUN;
	}
	/* false position method "heart" */
	for (nb_iter=0; nb_iter<FALSPOS_ITER; nb_iter++) {
		if (e_a-e_b == 0) {
			printf("Error itc_next\n");
			return NAN;
		}
		alpha_c = alpha_a - (alpha_a-alpha_b)/(e_a-e_b)*e_a;
		e_c = dist_straight(alphaD,alpha_c,chring_ptr) - CHAIN_STEP;
		if (e_a*e_c<0) {
			alpha_b = alpha_c;
			e_b = e_c;
		} else {
			alpha_a = alpha_c;
			e_a = e_c;
		}
		/* loop output if error is acceptable */
		if (fabs(e_c) < ERR_POSI) {
			break;
		}
	}
	/* next itc update */
	itc_update(idx+1,alpha_c,chring_ptr);
	return e_c;
}

double itc_place (t_chring* chring_ptr) {
	/* itcs position precise placement along curve
	sothat distance between 2 consecutive itcs equals chain step
	returned value is the maximum of optimisation residual error values
	*/
	int i;
	double error,errormax,angle;

	errormax = 0;
	/* first itc init */
	angle = 0;
	itc_update(0,angle,chring_ptr);
	/* and next ones computation
	n+1 itcs are computed, signed distance between the last and
	the first will be the error to be minimized for next step (scale tune)
	*/
	for (i=0; i<chring_ptr->teeth_nb; i++) {
		error = itc_next(i,chring_ptr);
		if (error==ERR_TUN) {
			return ERR_TUN;
		}
		if (fabs(error)>errormax) {
			errormax = fabs(error);
		}
	}
	return errormax;
}

int itc_round (t_chring* chring_ptr) {
	/* xys rounding to the µm for (usual) CNC machines */
	int i;
	double real,small,big;

	for (i=0; i<chring_ptr->teeth_nb+1; i++) {
		real = ((chring_ptr->itc_ptr+i)->x)*ROUND_SCALE;
		small = floor(real);
		big = ceil(real);
		if (fabs(small-real)<fabs(big-real)) {
			(chring_ptr->itc_ptr+i)->x_rnd = small / ROUND_SCALE;
		} else {
			(chring_ptr->itc_ptr+i)->x_rnd = big / ROUND_SCALE;
		}
		real = ((chring_ptr->itc_ptr+i)->y)*ROUND_SCALE;
		small = floor(real);
		big = ceil(real);
		if (fabs(small-real)<fabs(big-real)) {
			(chring_ptr->itc_ptr+i)->y_rnd = small / ROUND_SCALE;
		} else {
			(chring_ptr->itc_ptr+i)->y_rnd = big / ROUND_SCALE;
		}
	}
	return 0;
}

double itc_check (t_chring* chring_ptr) {
	/* check maximal distance error between
	2 consecutives itcs, including last and first */
	int i;
	double x1,y1,x2,y2;
	double error,maxerror;

	maxerror = 0;
	for (i=0; i<chring_ptr->teeth_nb-1; i++) {
		x1 = (chring_ptr->itc_ptr+i)->x;
		y1 = (chring_ptr->itc_ptr+i)->y;
		x2 = (chring_ptr->itc_ptr+i+1)->x;
		y2 = (chring_ptr->itc_ptr+i+1)->y;
		error = sqrt(pow((x1-x2),2) + pow((y1-y2),2)) - CHAIN_STEP;
		(chring_ptr->itc_ptr+i)->errnxt = error;
		if (fabs(error) > maxerror) {
			maxerror = fabs(error);
		}
	}
	x1 = (chring_ptr->itc_ptr+chring_ptr->teeth_nb-1)->x;
	y1 = (chring_ptr->itc_ptr+chring_ptr->teeth_nb-1)->y;
	x2 = (chring_ptr->itc_ptr+0)->x;
	y2 = (chring_ptr->itc_ptr+0)->y;
	error = sqrt(pow((x1-x2),2) + pow((y1-y2),2)) - CHAIN_STEP;
	(chring_ptr->itc_ptr+chring_ptr->teeth_nb-1)->errnxt = error;
	if (fabs(error) > maxerror) {
		maxerror = fabs(error);
	}
	return maxerror;
}

int scale_guess (int nbDents,t_chring* chring_ptr) {
	/* given a teeth number a first approximate value is "guessed"
	with the following idea : radius is proportional to curve perimeter
	*/
	double curvi,curvi_target,currentradius,radius;

	curvi_target = nbDents * CHAIN_STEP;
	currentradius = 100;
	radius_init (chring_ptr);
	chring_ptr->scale = currentradius;
	curvi = long_curvi(0,2*M_PI,chring_ptr);
	if (curvi == 0) {
		printf("Erreur scale_guess\n");
		chring_ptr->scale = NAN;
		return ERR_SCALEGUESS;
	}
	radius = currentradius * curvi_target / curvi;
	chring_ptr->scale = radius;
	return NO_ERR;
}

double scale_tune (t_chring* chring_ptr) {
    /* final optimisation of the scale factor so that le last tooth is
    located at a correct distance (one chain step) to the first tooth
	Beware ! of hypothesis
	    - enough itc (teeth number) are allocated in memory
	    - the first itc parameters are "realistic"
	returned value is the optimisation residual error values
	*/

	double r_a,r_b,r_c;
	double e_a,e_b,e_c;
	int nb_iter,d,iw;

	/* optimisation is performed by false position method (c.f. Wikipedia) */

	/* to compute e_a, e_b et e_c errors,
	one more than necessary itc is computed.This "last+1" is the one we would like
	to be at the same position as the first.
	The error is then curvilinear (signed) distance between first and "last+1" itcs
	 */

	/* initialisation */
	/* "a side" gives a negative error value */
	d = chring_ptr->teeth_nb;
	e_a = 1;
	iw = 0;
	while (e_a > 0) {
		d++;
		scale_guess(d,chring_ptr);
		r_a = chring_ptr->scale;
		if (itc_place(chring_ptr)==ERR_TUN) {
			return ERR_TUN;
		}
		e_a = dist_curvi(chring_ptr->itc_ptr->alpha,(chring_ptr->itc_ptr+chring_ptr->teeth_nb)->alpha,chring_ptr);
		iw++;
		if (iw>IW_MAX) { /* avoid infinite loop ! */
			return ERR_TUN;
		}
	}
	/* "b side" gives a positive error value */
	d = chring_ptr->teeth_nb;
	scale_guess(d,chring_ptr);
	r_b = chring_ptr->scale;
	e_b = -1;
	iw = 0;
	while (e_b < 0) {
		d--;
		scale_guess(d,chring_ptr);
		r_b = chring_ptr->scale;
		if (itc_place(chring_ptr)==ERR_TUN) {
			return ERR_TUN;
		}
		e_b = dist_curvi(chring_ptr->itc_ptr->alpha,(chring_ptr->itc_ptr+chring_ptr->teeth_nb)->alpha,chring_ptr);
		iw++;
		if (iw>IW_MAX) { /* avoid infinite loop ! */
			return ERR_TUN;
		}
	}

	/* check e_a and e_b are oppositely signed */
	if (e_a*e_b>=0) {
		return ERR_TUN;
	}
	/* false position method "heart" */
	for (nb_iter=0; nb_iter<FALSPOS_ITER; nb_iter++) {
		if (e_a-e_b == 0) {
			printf("Erreur scale_tune\n");
			return NAN;
		}
		r_c = r_a - (r_a-r_b)/(e_a-e_b)*e_a;
		chring_ptr->scale = r_c;

		if (itc_place(chring_ptr)==ERR_TUN) {
			return ERR_TUN;
		}
		e_c = dist_curvi(chring_ptr->itc_ptr->alpha,(chring_ptr->itc_ptr+chring_ptr->teeth_nb)->alpha,chring_ptr);
		if (e_a*e_c<0) {
			r_b = r_c;
			e_b = e_c;
		} else {
			r_a = r_c;
			e_a = e_c;
		}
		/* loop output if error is acceptable */
		if (fabs(e_c) < ERR_RADIUS) {
			break;
		}
	}
	return e_c;
}

