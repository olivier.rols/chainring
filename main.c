#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "tooth.h"
#include "dxf.h"
#include "ring.h"
#include "data.h"

int compute(t_data* data_ptr);
void print_err(int ret,char* msg);

int main (int argc, char *argv[]) {

	t_data mydata;
	t_chring mychring;
	t_bcd mybcd;
	char errmsg[100];
	int ret;

	switch (argc) {
	case 1:
		printf("Default configuration used \n");
		mydata.config_type = CONF_DEFAULT;
		break;
	case 2:
		printf("Using %s configuration file\n", argv[1]);
		mydata.config_type = CONF_FILE;
		mydata.config_filename = argv[1];
		break;
	default:
		printf("Usage : %s followed by a configuration filename, default (useless ?) config if not.\n", argv[0]);
		return 0;
		break;
	}

	mychring.bcd_ptr = &mybcd;
	mydata.chring_ptr = &mychring;
	ret = get_data(&mydata);
	if (ret != NO_ERR) {
		print_err(ret,errmsg);
		return 0;
	}
	ret = compute(&mydata);
	print_err(ret,errmsg);
	return 0;
}

int compute(t_data* data_ptr) {
	int ret;

	ret = NO_ERR;
	printf("1_");
	ret = rg_alloc(data_ptr->chring_ptr);
	if (ret != NO_ERR) {
		printf("Erreur rg_alloc\n");
		return 0;
	}
	printf("2_");
	ret = rg_open(data_ptr->chring_ptr,data_ptr->output_filename);
	if (ret != NO_ERR) {
		printf("Erreur rg_open\n");
		rg_free(data_ptr->chring_ptr);
		return 0;
	}
	printf("3_");
	ret = rg_itc_comp(data_ptr->chring_ptr);
	printf("4_");
	rg_teeth_comp(data_ptr->chring_ptr);
	printf("5_");
	ret = rg_teeth_write(data_ptr->chring_ptr);
	if (ret != NO_ERR) {
		printf("Erreur rg_teeth_write\n");
		rg_free(data_ptr->chring_ptr);
		rg_close(data_ptr->chring_ptr);
		return 0;
	}
	printf("6_");
	ret = rg_bcd_write(data_ptr->chring_ptr);
	if (ret != NO_ERR) {
		printf("Erreur rg_bcd_write\n");
		rg_free(data_ptr->chring_ptr);
		rg_close(data_ptr->chring_ptr);
		return 0;
	}
	rg_close(data_ptr->chring_ptr);
	printf("7_");
	rg_free(data_ptr->chring_ptr);
	printf("done !\n");
	printf("Maximal intrateeth error : %3.12lf µm\n",data_ptr->chring_ptr->num_error*1e3);
	return 0;
}

void print_err(int ret,char* msg) {
	switch (ret) {
	case NO_ERR:
		sprintf(msg,"Everything is fine.\n");
		break;
	case ERR_FILE:
		sprintf(msg,"File error : %d\n",ret);
		break;
	case ERR_ALLOC:
		sprintf(msg,"Memory error : %d\n",ret);
		break;
	case ERR_CONF_FILE:
		sprintf(msg,"Invalid data in configuration file : %d\n",ret);
		break;
	default :
		sprintf(msg,"Unknown error : %d\n",ret);
	}
	printf("%s\n",msg);
}
