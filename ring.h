#ifndef RING_H_INCLUDED
#define RING_H_INCLUDED

#define CHAIN_STEP 12.7
#define ROLL_RADIUS 4
/* default excentricity and teeth height */
#define DEF_EXC 1.23165
#define DEF_HEIGHT 2
#define MAX_HEIGHT (sqrt(pow(CHAIN_STEP-ROLL_RADIUS,2) - pow(CHAIN_STEP,2)/4))
/* BCD default parameters */
#define DEF_ARM_CLEARANCE 3
#define DEF_HOLES_CNT 25
#define BCD_YES 1
#define BCD_NO 0
#define OSYM_1STHOLE_ANGLE 51

/* error codes */
#define NO_ERR 0
#define ERR_ARGS (NO_ERR-1)
#define ERR_FILE (ERR_ARGS-1)
#define ERR_WRITE (ERR_FILE-1)
#define ERR_ALLOC (ERR_WRITE-1)
#define ERR_TUN (ERR_ALLOC-1)
#define ERR_SCALEGUESS (ERR_TUN-1)
#define ERR_CONF_FILE (ERR_SCALEGUESS-1)

typedef struct CHAINRING {
	FILE* file;
	t_angularZone* agzone_ptr;
	double exc;
	double scale;
	int teeth_nb;
	double teeth_height;
	t_itc* itc_ptr;
	t_tooth* teeth_ptr;
	t_bcd* bcd_ptr;
	double num_error;
} t_chring;

int rg_alloc(t_chring* chring_ptr);
int rg_open(t_chring* chring_ptr,const char* filename);
int rg_itc_comp(t_chring* chring_ptr);
void rg_teeth_comp(t_chring* chring_ptr);
int rg_teeth_write(t_chring* chring_ptr);
int rg_bcd_write(t_chring* chring_ptr);
int rg_close(t_chring* chring_ptr);
void rg_free(t_chring* chring_ptr);

#endif /* RING_H_INCLUDED */
