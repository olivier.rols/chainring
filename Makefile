CC=gcc
CFLAGS=-W -Wall -ansi -pedantic -std=gnu99 -g
LDFLAGS= -lm
EXEC=chainring
SRC= $(wildcard *.c)
OBJ= $(SRC:.c=.o)

all: $(EXEC)

$(EXEC): $(OBJ)
	@$(CC) -o $@ $^ $(LDFLAGS)


%.o: %.c
	@$(CC) -o $@ -c $< $(CFLAGS)

clean:
	rm $(OBJ) $(EXEC)