﻿#include <stdio.h>
#include <math.h>

#include "tooth.h"
#include "dxf.h"
#include "ring.h"
#include "itc.h"
#include "radius.h"

void radius_init (t_chring* chring_ptr) {

	/* magical figures init */
	(chring_ptr->agzone_ptr+0)->alphainf = 0.0 * M_PI / 180;
	(chring_ptr->agzone_ptr+0)->alphasup = 30.0 * M_PI / 180;
	(chring_ptr->agzone_ptr+0)->deltaR = 2.56/100;

	(chring_ptr->agzone_ptr+1)->alphainf = (chring_ptr->agzone_ptr+0)->alphasup;
	(chring_ptr->agzone_ptr+1)->alphasup = 90.0 * M_PI / 180;
	(chring_ptr->agzone_ptr+1)->deltaR = 18.94/100;

	(chring_ptr->agzone_ptr+2)->alphainf = (chring_ptr->agzone_ptr+1)->alphasup;
	(chring_ptr->agzone_ptr+2)->alphasup = 107.0 * M_PI / 180;
	(chring_ptr->agzone_ptr+2)->deltaR = DEF_EXC - 1.0 -
	                                     ((chring_ptr->agzone_ptr+0)->deltaR+(chring_ptr->agzone_ptr+1)->deltaR);

	(chring_ptr->agzone_ptr+3)->alphainf = (chring_ptr->agzone_ptr+2)->alphasup;
	(chring_ptr->agzone_ptr+3)->alphasup = 143.0 * M_PI / 180;
	(chring_ptr->agzone_ptr+3)->deltaR = -4.2/100;

	(chring_ptr->agzone_ptr+4)->alphainf = (chring_ptr->agzone_ptr+3)->alphasup;
	(chring_ptr->agzone_ptr+4)->alphasup = 180.0 * M_PI / 180;
	(chring_ptr->agzone_ptr+4)->deltaR = - ((chring_ptr->agzone_ptr+0)->deltaR+(chring_ptr->agzone_ptr+1)->deltaR +
	                                        (chring_ptr->agzone_ptr+2)->deltaR+(chring_ptr->agzone_ptr+3)->deltaR);
}

double radius_n (double alpha, t_chring* chring_ptr) {
	/* normalised radius computation for a given angle
	    angle is between 0 and 2PI
	    minimal radius = 1
	    maximal radius = exc (total excentricity)
	    */

	double r,r1,r2;
	double nbtours;

	/* obvious case = circular chainring
	nno need to compute anything ! */
	if (chring_ptr->exc == 1.0) {
		return 1.0;
	}

	/* computations */
	r = NAN;

	/* get angle between 0 and PI because half turn symetry is supposed */
	nbtours = floor(alpha / (2*M_PI));
	alpha = alpha - 2*M_PI*nbtours;
	if (alpha >= (chring_ptr->agzone_ptr+4)->alphasup) {
		alpha = alpha - (chring_ptr->agzone_ptr+4)->alphasup;
	}

	/* radius computation given in which zone we are */
	if (alpha >= (chring_ptr->agzone_ptr+0)->alphainf && alpha < (chring_ptr->agzone_ptr+0)->alphasup) {
		r = 1+
		    (chring_ptr->agzone_ptr+0)->deltaR * (alpha - (chring_ptr->agzone_ptr+0)->alphainf) /
		    ((chring_ptr->agzone_ptr+0)->alphasup - (chring_ptr->agzone_ptr+0)->alphainf);
	}
	if (alpha >= (chring_ptr->agzone_ptr+1)->alphainf && alpha < (chring_ptr->agzone_ptr+1)->alphasup) {
		r = 1+ (chring_ptr->agzone_ptr+0)->deltaR +
		    (chring_ptr->agzone_ptr+1)->deltaR * sin (alpha) * (alpha - (chring_ptr->agzone_ptr+1)->alphainf) /
		    ((chring_ptr->agzone_ptr+1)->alphasup - (chring_ptr->agzone_ptr+1)->alphainf);
	}
	if (alpha >= (chring_ptr->agzone_ptr+2)->alphainf && alpha < (chring_ptr->agzone_ptr+2)->alphasup) {
		r = 1+ (chring_ptr->agzone_ptr+0)->deltaR + (chring_ptr->agzone_ptr+1)->deltaR +
		    (chring_ptr->agzone_ptr+2)->deltaR * (alpha - (chring_ptr->agzone_ptr+2)->alphainf) /
		    ((chring_ptr->agzone_ptr+2)->alphasup - (chring_ptr->agzone_ptr+2)->alphainf);
	}
	if (alpha >= (chring_ptr->agzone_ptr+3)->alphainf && alpha < (chring_ptr->agzone_ptr+3)->alphasup) {
		r = 1+ (chring_ptr->agzone_ptr+0)->deltaR + (chring_ptr->agzone_ptr+1)->deltaR +
		    (chring_ptr->agzone_ptr+2)->deltaR +
		    (chring_ptr->agzone_ptr+3)->deltaR * (alpha - (chring_ptr->agzone_ptr+3)->alphainf) /
		    ((chring_ptr->agzone_ptr+3)->alphasup - (chring_ptr->agzone_ptr+3)->alphainf);
	}
	if (alpha >= (chring_ptr->agzone_ptr+4)->alphainf && alpha <= (chring_ptr->agzone_ptr+4)->alphasup) {
		r1 = 1+ (chring_ptr->agzone_ptr+0)->deltaR + (chring_ptr->agzone_ptr+1)->deltaR +
		     (chring_ptr->agzone_ptr+2)->deltaR +
		     (chring_ptr->agzone_ptr+3)->deltaR * (alpha - (chring_ptr->agzone_ptr+3)->alphainf) /
		     ((chring_ptr->agzone_ptr+3)->alphasup - (chring_ptr->agzone_ptr+3)->alphainf);

		r2 = 1 / cos (M_PI - alpha);
		if (r1 < r2) r = r1;
		else r = r2;
	}
	/* and forget not parametrable ecentricity management */
	return 1.0 + chring_ptr->exc * (r - 1)/DEF_EXC;
}
