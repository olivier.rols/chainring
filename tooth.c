#include <stdio.h>
#include <math.h>

#include "tooth.h"
#include "ring.h"

void roll_comp (t_tooth* tooth,int teeth_nb) {
	t_arc myarc;
	double deltax,deltay,alpha;

	myarc.xc = tooth->itc_ptr->x;
	myarc.yc = tooth->itc_ptr->y;
	myarc.r = ROLL_RADIUS;

	/* first angle between next itc and current */
	/* as ther is one more itc than teeth nuumber
	the next always exists */
	deltax = (tooth->itc_ptr+1)->x - myarc.xc;
	deltay = (tooth->itc_ptr+1)->y - myarc.yc;
	alpha = atan2(deltay,deltax);
	myarc.ad = alpha;

	/* secaond angle between next itc and current */
	/* first itc detection */
	if (tooth->itc_ptr->idx == 0) {
		/* the former is in fact the last one */
		deltax = (tooth->itc_ptr + teeth_nb - 1)->x - myarc.xc;
		deltay = (tooth->itc_ptr + teeth_nb - 1)->y - myarc.yc;
	} else {
		/* not on the first itc, find the former directly */
		deltax = (tooth->itc_ptr-1)->x - myarc.xc;
		deltay = (tooth->itc_ptr-1)->y - myarc.yc;
	}
	alpha = atan2(deltay,deltax);
	myarc.af = alpha;

	/* results updates */
	tooth->roll.xc = myarc.xc;
	tooth->roll.yc = myarc.yc;
	tooth->roll.r = myarc.r;
	tooth->roll.ad = myarc.ad;
	tooth->roll.af = myarc.af;
}

void up_dev_comp (t_tooth* tooth,double teeth_height) {
	double chainstep,dx,dy;
	t_arc myarc;

	myarc.xc = (tooth->itc_ptr+1)->x;
	myarc.yc = (tooth->itc_ptr+1)->y;
	dx = (tooth->itc_ptr+1)->x - (tooth->itc_ptr)->x;
	dy = (tooth->itc_ptr+1)->y - (tooth->itc_ptr)->y;
	chainstep = sqrt(pow(dx, 2)+pow(dy, 2));
	myarc.r = chainstep - tooth->roll.r;
	myarc.ad = tooth->roll.ad - M_PI;
	myarc.af = myarc.ad + asin(teeth_height/myarc.r);

	/* results updates */
	tooth->up_dev.xc = myarc.xc;
	tooth->up_dev.yc = myarc.yc;
	tooth->up_dev.r = myarc.r;
	tooth->up_dev.ad = myarc.ad;
	tooth->up_dev.af = myarc.af;
}

void head_comp (t_tooth* tooth) {
	t_line myline;

	myline.xd = (tooth->itc_ptr+1)->x + (tooth->up_dev.r)*cos(tooth->up_dev.af);
	myline.yd = (tooth->itc_ptr+1)->y + (tooth->up_dev.r)*sin(tooth->up_dev.af);
	myline.xf = (tooth->itc_ptr)->x + (tooth->dwn_dev.r)*cos(tooth->dwn_dev.ad);
	myline.yf = (tooth->itc_ptr)->y + (tooth->dwn_dev.r)*sin(tooth->dwn_dev.ad);

	/* results updates */
	tooth->head.xd = myline.xd;
	tooth->head.yd = myline.yd;
	tooth->head.xf = myline.xf;
	tooth->head.yf = myline.yf;
}

void dwn_dev_comp (t_tooth* tooth) {
	t_arc myarc;

	myarc.xc = (tooth->itc_ptr)->x;
	myarc.yc = (tooth->itc_ptr)->y;
	myarc.r = tooth->up_dev.r;
	myarc.ad = tooth->roll.ad - (tooth->up_dev.af - tooth->up_dev.ad);
	myarc.af = tooth->roll.ad;

	/* results updates */
	tooth->dwn_dev.xc = myarc.xc;
	tooth->dwn_dev.yc = myarc.yc;
	tooth->dwn_dev.r = myarc.r;
	tooth->dwn_dev.ad = myarc.ad;
	tooth->dwn_dev.af = myarc.af;
}

