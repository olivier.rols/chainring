#ifndef DXF_H_INCLUDED
#define DXF_H_INCLUDED

/* dxf consts */
#define DXF_HEADER "999\nDXF created from OVale\n0\nSECTION\n2\nHEADER\n9\n$ACADVER\n1\nAC1015\n0\nENDSEC\n"
#define DXF_TABLES "0\nSECTION\n2\nTABLES\n0\nENDSEC\n"
#define DXF_BLOCKS "0\nSECTION\n2\nBLOCKS\n0\nENDSEC\n"
#define DXF_ENTITIES_START "0\nSECTION\n2\nENTITIES\n"
#define DXF_ENTITIES_STOP "0\nENDSEC\n"
#define DXF_LINE "0\nLINE\n10\n%f\n20\n%f\n11\n%f\n21\n%f\n"
#define DXF_ARC "0\nARC\n10\n%f\n20\n%f\n40\n%f\n50\n%f\n51\n%f\n"
#define DXF_CIRCLE "0\nCIRCLE\n10\n%f\n20\n%f\n40\n%f\n"

/* only three types of dxf entites need to be writen */
int dxfwrite_line (FILE* fp, t_line* line);
int dxfwrite_arc (FILE* fp, t_arc* arc);
int dxfwrite_circle (FILE* fp, t_circle* circle);

#endif /* DXF_H_INCLUDED */
