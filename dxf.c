#include <stdio.h>
#include <math.h>

#include "tooth.h"
#include "dxf.h"

int dxfwrite_line (FILE* fp, t_line* line) {
	return fprintf (fp,DXF_LINE,line->xd,line->yd,line->xf,line->yf);
}

int dxfwrite_arc (FILE* fp, t_arc* arc) {
	return fprintf (fp,DXF_ARC,arc->xc,arc->yc,arc->r,arc->ad*180/M_PI,arc->af*180/M_PI);
}

int dxfwrite_circle (FILE* fp, t_circle* circle) {
	return fprintf (fp,DXF_CIRCLE,circle->xc,circle->yc,circle->r);
}

