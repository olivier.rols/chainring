#ifndef RADIUS_H_INCLUDED
#define RADIUS_H_INCLUDED

#define AGZONE_NB 5

void radius_init (t_chring* chring_ptr);
double radius_n (double alpha, t_chring* chring_ptr);

#endif /* RADIUS_H_INCLUDED */
