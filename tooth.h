#ifndef TOOTH_H_INCLUDED
#define TOOTH_H_INCLUDED

typedef struct LINE {
	/* line coordinates for begin (d) and end (f)*/
	double xd;
	double yd;
	double xf;
	double yf;
} t_line;

typedef struct ARC {
	/* arc : coordinates for centre, radius begin and end angle (in trigonmetric direction) */
	double xc;
	double yc;
	double r;
	double ad;
	double af;
} t_arc;

typedef struct CIRCLE {
	/* circle : center coordinates, radius */
	double xc;
	double yc;
	double r;
} t_circle;

typedef struct ITC {
    /* Inter Teeth Circle */
	double x;
	double y;
	double x_rnd;
	double y_rnd;
	double r;
	double alpha;
	double errnxt;  /* distance to next error */
	int idx;    /* index for list management */
} t_itc;

typedef struct TOOTH {
	/* tooth : path to increasing angles direction
	    - current itc pointer (! supposed to be allocated !)
	    - arc for chain roll
	    - ascending tooth arc
	    - head line
	    - descending tooth arc
	*/
	t_itc* itc_ptr;
	t_arc roll;
	t_arc up_dev;
	t_line head;
	t_arc dwn_dev;
} t_tooth;

typedef struct BCD {
	/* Bolt Circle Diamater :
	    - "Values" are 110, 130 etc ... (nomi_dia)
	    - number of arms (4 or 5)
	    - number of holes (constant spece)
	    - holes diameter
	    - first hole angle
	*/
	int tobe_drawn;
	double nomi_dia;
	int nb_arms;
	int holes_cnt;
	double holes_dia;
	double first_hole_alpha;
} t_bcd;

/* zones parameters as descibed within OSym pattent (WO/1993/007044) */
typedef struct zone {
	double alphainf, alphasup;
	double deltaR;
} t_angularZone;

void roll_comp (t_tooth* tooth,int teeth_nb);
void up_dev_comp (t_tooth* tooth,double teeth_height);
void head_comp (t_tooth* tooth);
void dwn_dev_comp (t_tooth* tooth);

#endif /* TOOTH_H_INCLUDED */
