#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "tooth.h"
#include "dxf.h"
#include "ring.h"
#include "itc.h"
#include "radius.h"

int rg_alloc(t_chring* chring_ptr) {
	int i;

	/* memory allocation + freeing in case of error */
	chring_ptr->itc_ptr = (t_itc*) malloc((chring_ptr->teeth_nb + 1) * sizeof(t_itc));
	if (chring_ptr->itc_ptr == NULL) return ERR_ALLOC;
	chring_ptr->teeth_ptr = (t_tooth*) malloc((chring_ptr->teeth_nb) * sizeof(t_tooth));
	if (chring_ptr->teeth_ptr == NULL) {
		free(chring_ptr->itc_ptr);
		return ERR_ALLOC;
	}
	chring_ptr->agzone_ptr = (t_angularZone*) malloc(AGZONE_NB * sizeof(t_angularZone));
	if (chring_ptr->agzone_ptr == NULL) {
		free(chring_ptr->teeth_ptr);
		free(chring_ptr->itc_ptr);
		return ERR_ALLOC;
	}

	/* links init and indexe */
	for (i=0; i<chring_ptr->teeth_nb; i++) {
		(chring_ptr->itc_ptr+i)->idx = i;
		(chring_ptr->teeth_ptr+i)->itc_ptr = chring_ptr->itc_ptr+i;
	}
	(chring_ptr->itc_ptr+chring_ptr->teeth_nb+1)->idx = chring_ptr->teeth_nb+1;
	return NO_ERR;
}

void rg_free(t_chring* chring_ptr) {
	free(chring_ptr->teeth_ptr);
	free(chring_ptr->itc_ptr);
}

int rg_open(t_chring* chring_ptr,const char* filename) {
	FILE* fp;

	fp = fopen(filename,"w");
	if (fp == NULL) {
		return ERR_FILE;
	}
	fprintf (fp,DXF_HEADER);
	fprintf (fp,DXF_TABLES);
	fprintf (fp,DXF_BLOCKS);
	fprintf (fp,DXF_ENTITIES_START);
	chring_ptr->file = fp;
	return NO_ERR;
}

int rg_close(t_chring* chring_ptr) {
	FILE* fp;

	fp = chring_ptr->file;
	fprintf (fp,DXF_ENTITIES_STOP);
	return fclose (fp);
}

int rg_itc_comp(t_chring* chring_ptr) {
	double erreur,angle;

	/* zones parametrisation for computing */

	/* first itc init */
	angle = 0;
	scale_guess(chring_ptr->teeth_nb,chring_ptr);
	itc_update(0,angle,chring_ptr);

	/* and followers placement */
	erreur = scale_tune (chring_ptr);
	if (erreur==ERR_TUN) {
		printf("Erreur scale_tune\n");
		return ERR_TUN;
	}
	/* verification */
	erreur = itc_check (chring_ptr);

	/* µm rounding */
	itc_round(chring_ptr);
	erreur = itc_check (chring_ptr);
	chring_ptr->num_error = erreur;
	return NO_ERR;
}

void rg_teeth_comp(t_chring* chring_ptr) {
	int i;

	for (i=0; i<chring_ptr->teeth_nb; i++) {
		/* it is important to keep this order because
		*_comp computation assume that former
		results are valid */
		roll_comp(chring_ptr->teeth_ptr+i,chring_ptr->teeth_nb);
		up_dev_comp(chring_ptr->teeth_ptr+i,chring_ptr->teeth_height);
		dwn_dev_comp(chring_ptr->teeth_ptr+i);
		head_comp(chring_ptr->teeth_ptr+i);
	}
}

int rg_teeth_write(t_chring* chring_ptr) {
	int err;
	int i;
	FILE* fp;

	fp = chring_ptr->file;
	err = NO_ERR;
	for (i=0; i<chring_ptr->teeth_nb; i++) {
		err = dxfwrite_arc(fp,&((chring_ptr->teeth_ptr+i)->roll));
		if (err < 0) {
			return ERR_WRITE;
		}
		err = dxfwrite_arc(fp,&((chring_ptr->teeth_ptr+i)->up_dev));
		if (err < 0) {
			return ERR_WRITE;
		}
		err = dxfwrite_line(fp,&((chring_ptr->teeth_ptr+i)->head));
		if (err < 0) {
			return ERR_WRITE;
		}
		err = dxfwrite_arc(fp,&((chring_ptr->teeth_ptr+i)->dwn_dev));
		if (err < 0) {
			return ERR_WRITE;
		}
	}
	return NO_ERR;
}

int rg_bcd_write(t_chring* chring_ptr) {
	int err;
	int i,idx,bcd_nb;
	FILE* fp;
	double angle,angle0,d_angle;
	t_circle mycircle;

	err = NO_ERR;
	if (chring_ptr->bcd_ptr->tobe_drawn == BCD_NO) {
		return NO_ERR;
	}
	fp = chring_ptr->file;
	mycircle.r = chring_ptr->bcd_ptr->holes_dia / 2;
	angle0 = chring_ptr->bcd_ptr->first_hole_alpha * M_PI/180;
	d_angle = (2*M_PI / chring_ptr->bcd_ptr->nb_arms);
	/* BCD holes drawing */
	/* position number between two chainring arms */
	bcd_nb = chring_ptr->bcd_ptr->holes_cnt / chring_ptr->bcd_ptr->nb_arms;
	for (idx=0; idx<bcd_nb; idx++) {
		angle0 = angle0 + 2*M_PI / chring_ptr->bcd_ptr->holes_cnt;
		/* each hole group (nb_arms) */
		for (i=0; i<chring_ptr->bcd_ptr->nb_arms; i++) {
			angle = angle0 + d_angle*i;
			mycircle.xc = chring_ptr->bcd_ptr->nomi_dia / 2 * cos (angle);
			mycircle.yc = chring_ptr->bcd_ptr->nomi_dia / 2 * sin (angle);
			err = dxfwrite_circle (fp, &mycircle);
			if (err < 0) {
				return ERR_WRITE;
			}
		}
	}
	/* arms main hole drawing */
	mycircle.r = chring_ptr->bcd_ptr->nomi_dia/2 - chring_ptr->bcd_ptr->holes_dia/2 - DEF_ARM_CLEARANCE;
	mycircle.xc = 0;
	mycircle.yc = 0;
	err = dxfwrite_circle (fp, &mycircle);
	if (err < 0) {
		return ERR_WRITE;
	}
	return NO_ERR;
}

