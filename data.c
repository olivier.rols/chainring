#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "tooth.h"
#include "ring.h"
#include "data.h"

int get_data(t_data* data_ptr) {

	/* config file parameter keys */
	char* k0 = "TEETH_NBR=";
	char* k1 = "OUTPUT_FILE_NAME=";
	char* k2 = "CHAIN_STEP=";
	char* k3 = "ROLL_RADIUS=";
	char* k4 = "EXCENTRICITY=";
	char* k5 = "TEETH_HEIGHT=";
	char* k6 = "ARM_CLEARANCE=";
	char* k7 = "HOLES_CNT=";
	char* k8 = "1STHOLE_ANGLE=";
	char* k9 = "BCD=";
	char* k10 = "NB_ARMS=";
	char* k11 = "HOLES_DIA=";
	char* k12 = "DRAW_BCD=";
	char* constkeylist[] = {k0,k1,k2,k3,k4,k5,k6,k7,k8,k9,k10,k11,k12};

	int ret;

	ret = NO_ERR;
	data_ptr->keylist = constkeylist;
	/* default values are set ... basicaly ;-) */
	sprintf (data_ptr->output_filename,"def_out.dxf");
	data_ptr->chring_ptr->exc = 1.0 * DEF_EXC;
	data_ptr->chring_ptr->teeth_nb = 72;
	data_ptr->chring_ptr->teeth_height = DEF_HEIGHT;
	data_ptr->chring_ptr->bcd_ptr->nomi_dia = 110;
	data_ptr->chring_ptr->bcd_ptr->nb_arms = 5;
	data_ptr->chring_ptr->bcd_ptr->holes_cnt = DEF_HOLES_CNT;
	data_ptr->chring_ptr->bcd_ptr->holes_dia = 10;
	data_ptr->chring_ptr->bcd_ptr->first_hole_alpha = OSYM_1STHOLE_ANGLE;
	data_ptr->chring_ptr->bcd_ptr->tobe_drawn = BCD_YES;

	/* ... and might be fullfilled by a dedicated configuration file */
	if (data_ptr->config_type == CONF_FILE) {
		ret = config_parse(data_ptr);
	}
	return ret;
}

int config_parse(t_data* data_ptr) {
	FILE* fd;
	char buf[LINE_MAX_SIZE];
	int idx;
	int ret;

	ret = NO_ERR;
	fd = fopen(data_ptr->config_filename,"r");
	if (fd == NULL) {
		return ERR_FILE;
	}

	while (fgets (buf,LINE_MAX_SIZE,fd) != NULL) {
		/* ignore empty lines and comments, which begin with # as well */
		if ((strlen(buf) > 1) && (strncmp(buf,"#",1) != 0)) {
			/* search for an "=" sign */
			idx = 0;
			while ((strncmp(buf+idx,"=",1) != 0) && (idx < LINE_MAX_SIZE)) {
				idx++;
			}
			if (idx < LINE_MAX_SIZE) {
				/* an "=" sign if found, extract data */
				ret += data_extract (data_ptr,buf,idx);
			}
		}
	}
	fclose (fd);
	/* if, at least, one data is not valid, return error */
	if (ret!=0) ret=ERR_CONF_FILE;
	return ret ;
}

int data_extract (t_data* data_ptr,char* buf, int eqpos) {

	int idx;

	/* systematic key name detection, corresponding parameter converion and store at the right place */
	if (strncmp(data_ptr->keylist[0],buf,eqpos) == 0) {
		/* TEETH_NBR */
		data_ptr->chring_ptr->teeth_nb = atoi(buf+eqpos+1);
		return NO_ERR;
	}
	if (strncmp(data_ptr->keylist[1],buf,eqpos) == 0) {
		/* OUTPUT_FILE_NAME */
		strcpy (data_ptr->output_filename,buf+eqpos+1);
		/* Quick and (very !!) dirty UTF-8 management */
		idx = strlen (data_ptr->output_filename);
		data_ptr->output_filename [idx-1] =  0x00 ;
		return NO_ERR;
	}
	if (strncmp(data_ptr->keylist[2],buf,eqpos) == 0) {
		/* CHAIN_STEP ... standard fixed value for the whole cycle industry, no modif needed */
		return NO_ERR;
	}
	if (strncmp(data_ptr->keylist[3],buf,eqpos) == 0) {
		/* ROLL_RADIUS ... standard fixed value for the whole cycle industry, no modif needed  */
		return NO_ERR;
	}
	if (strncmp(data_ptr->keylist[4],buf,eqpos) == 0) {
		/* EXCENTRICITY */
		data_ptr->chring_ptr->exc = atof(buf+eqpos+1);
		return NO_ERR;
	}
	if (strncmp(data_ptr->keylist[5],buf,eqpos) == 0) {
		/* TEETH_HEIGHT */
		data_ptr->chring_ptr->teeth_height = atof(buf+eqpos+1);
		return NO_ERR;
	}
	if (strncmp(data_ptr->keylist[6],buf,eqpos) == 0) {
		/* ARM_CLEARANCE ... TODO, default ring.h const value for the moment */
		return NO_ERR;
	}
	if (strncmp(data_ptr->keylist[7],buf,eqpos) == 0) {
		/* HOLES_CNT */
		data_ptr->chring_ptr->bcd_ptr->holes_cnt = atof(buf+eqpos+1);
		return NO_ERR;
	}
	if (strncmp(data_ptr->keylist[8],buf,eqpos) == 0) {
		/* 1STHOLE_ANGLE */
		data_ptr->chring_ptr->bcd_ptr->first_hole_alpha = atof(buf+eqpos+1);
		return NO_ERR;
	}
	if (strncmp(data_ptr->keylist[9],buf,eqpos) == 0) {
		/* BCD */
		data_ptr->chring_ptr->bcd_ptr->nomi_dia = atof(buf+eqpos+1);
		return NO_ERR;
	}
	if (strncmp(data_ptr->keylist[10],buf,eqpos) == 0) {
		/* NB_ARMS */
		data_ptr->chring_ptr->bcd_ptr->nb_arms = atof(buf+eqpos+1);
		return NO_ERR;
	}
	if (strncmp(data_ptr->keylist[11],buf,eqpos) == 0) {
		/* HOLES_DIA */
		data_ptr->chring_ptr->bcd_ptr->holes_dia = atof(buf+eqpos+1);
		return NO_ERR;
	}
	if (strncmp(data_ptr->keylist[12],buf,eqpos) == 0) {
		/* DRAW_BCD */
		if (atof(buf+eqpos+1)!= 0) {
			data_ptr->chring_ptr->bcd_ptr->tobe_drawn = BCD_YES;
		} else {
			data_ptr->chring_ptr->bcd_ptr->tobe_drawn = BCD_NO;
		}
		return NO_ERR;
	}
	/* unknown key */
	return ERR_CONF_FILE;
}

