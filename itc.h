#ifndef ITC_H_INCLUDED
#define ITC_H_INCLUDED

#define CURVISTEP 3600


int scale_guess (int nbDents,t_chring* chring_ptr);
double scale_tune (t_chring* chring_ptr);
int itc_update (int idx, double alpha, t_chring* chring_ptr);
int itc_round (t_chring* chring_ptr);
double itc_check (t_chring* chring_ptr);

#endif /* ITC_H_INCLUDED */
